
player_attachto = {}

local pat = player_attachto -- short alias


-- What the data looks like
player_attachto = {
  player = {
    child = "object reference here",
    groups = {nogroup = 1, lock = 0},
  }
}
player_attachto = {}


function player_attachto.set_attach(_player, _child)
  pat[_player] = {child = _child}
  if _child._attach_groups ~= nil then
    pat[_player].groups = _child._attach_groups
  else
    pat[_player].groups = {nogroup = 1}
  end
end

function player_attachto.get_attach(_player)
  return pat[_player].child
end

function player_attachto.get_groups(_player)
  if pat[_player] and pat[_player].groups then
    return pat[_player].groups
  end
  return {nogroup = 1}
end

function player_attachto.has_group(_player, _group)
  if pat[_player] and pat[_player].groups
  and pat[_player].groups[_group] ~= nil then
    return pat[_player].groups[_group]
  end
  return 0
end
